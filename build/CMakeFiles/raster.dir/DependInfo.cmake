# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/brandon/Downloads/computer-graphics-raster-images-master/main.cpp" "/home/brandon/Downloads/computer-graphics-raster-images-master/build/CMakeFiles/raster.dir/main.cpp.o"
  "/home/brandon/Downloads/computer-graphics-raster-images-master/src/demosaic.cpp" "/home/brandon/Downloads/computer-graphics-raster-images-master/build/CMakeFiles/raster.dir/src/demosaic.cpp.o"
  "/home/brandon/Downloads/computer-graphics-raster-images-master/src/desaturate.cpp" "/home/brandon/Downloads/computer-graphics-raster-images-master/build/CMakeFiles/raster.dir/src/desaturate.cpp.o"
  "/home/brandon/Downloads/computer-graphics-raster-images-master/src/hsv_to_rgb.cpp" "/home/brandon/Downloads/computer-graphics-raster-images-master/build/CMakeFiles/raster.dir/src/hsv_to_rgb.cpp.o"
  "/home/brandon/Downloads/computer-graphics-raster-images-master/src/hue_shift.cpp" "/home/brandon/Downloads/computer-graphics-raster-images-master/build/CMakeFiles/raster.dir/src/hue_shift.cpp.o"
  "/home/brandon/Downloads/computer-graphics-raster-images-master/src/over.cpp" "/home/brandon/Downloads/computer-graphics-raster-images-master/build/CMakeFiles/raster.dir/src/over.cpp.o"
  "/home/brandon/Downloads/computer-graphics-raster-images-master/src/reflect.cpp" "/home/brandon/Downloads/computer-graphics-raster-images-master/build/CMakeFiles/raster.dir/src/reflect.cpp.o"
  "/home/brandon/Downloads/computer-graphics-raster-images-master/src/rgb_to_gray.cpp" "/home/brandon/Downloads/computer-graphics-raster-images-master/build/CMakeFiles/raster.dir/src/rgb_to_gray.cpp.o"
  "/home/brandon/Downloads/computer-graphics-raster-images-master/src/rgb_to_hsv.cpp" "/home/brandon/Downloads/computer-graphics-raster-images-master/build/CMakeFiles/raster.dir/src/rgb_to_hsv.cpp.o"
  "/home/brandon/Downloads/computer-graphics-raster-images-master/src/rgba_to_rgb.cpp" "/home/brandon/Downloads/computer-graphics-raster-images-master/build/CMakeFiles/raster.dir/src/rgba_to_rgb.cpp.o"
  "/home/brandon/Downloads/computer-graphics-raster-images-master/src/rotate.cpp" "/home/brandon/Downloads/computer-graphics-raster-images-master/build/CMakeFiles/raster.dir/src/rotate.cpp.o"
  "/home/brandon/Downloads/computer-graphics-raster-images-master/src/simulate_bayer_mosaic.cpp" "/home/brandon/Downloads/computer-graphics-raster-images-master/build/CMakeFiles/raster.dir/src/simulate_bayer_mosaic.cpp.o"
  "/home/brandon/Downloads/computer-graphics-raster-images-master/src/write_ppm.cpp" "/home/brandon/Downloads/computer-graphics-raster-images-master/build/CMakeFiles/raster.dir/src/write_ppm.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../include"
  "../json"
  "../shared/json"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
