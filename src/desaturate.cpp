#include "desaturate.h"
#include "hsv_to_rgb.h"
#include "rgb_to_hsv.h"
#include <cmath>

void desaturate(
  const std::vector<unsigned char> & rgb,
  const int width,
  const int height,
  const double factor,
  std::vector<unsigned char> & desaturated)
{
  desaturated.resize(rgb.size());
  ////////////////////////////////////////////////////////////////////////////
  // Add your code here
  for (int i=0; i<rgb.size(); i+=3){
      double r = rgb[i];
      double g = rgb[i+1];
      double b = rgb[i+2];
      double h;
      double s;
      double v;

      rgb_to_hsv(r,g,b,h,s,v);

      s = s - s*factor;

      hsv_to_rgb(h,s,v,r,g,b);

      desaturated[i] = r;
      desaturated[i+1] = g;
      desaturated[i+2] = b;
  }
  ////////////////////////////////////////////////////////////////////////////
}
