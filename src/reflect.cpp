#include "reflect.h"

void reflect(
  const std::vector<unsigned char> & input,
  const int width,
  const int height,
  const int num_channels,
  std::vector<unsigned char> & reflected)
{

  //start from the end of the row on the input, start at the beginning of the row 
  //for the output, heights are the same
  reflected.resize(width*height*num_channels);
  for(int i = 0; i < height; i++){
  	int reflect = width*num_channels;
  	for(int j = 0; j<width*num_channels; j+= num_channels){
  		for(int z = 0; z < num_channels; z++){
  			reflected[i*width*num_channels + j + z] = input[i*width*num_channels + reflect + z - num_channels];
  		}
  		reflect -= num_channels;
  	}
  }
}
