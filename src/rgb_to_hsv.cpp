#include "rgb_to_hsv.h"
#include <algorithm>
#include <cmath>

void rgb_to_hsv(
  const double r,
  const double g,
  const double b,
  double & h,
  double & s,
  double & v)
{
  ////////////////////////////////////////////////////////////////////////////
  // Replace with your code here:

  //prime values
  double r_prime = r/255;
  double g_prime = g/255;
  double b_prime = b/255;

  double cmax = std::max(r_prime, g_prime);
  cmax = std::max(cmax, b_prime);
  double cmin = std::min(r_prime, g_prime);
  cmin = std::min(cmin, b_prime);
  double delta = cmax - cmin;

  //https://www.rapidtables.com/convert/color/rgb-to-hsv.html
  //https://en.wikipedia.org/wiki/HSL_and_HSV
  if (cmax == 0){
      h = 0;
  }

  else if(cmax ==r_prime){
      double numerator = (g_prime - b_prime)/delta;
      h = 60*(std::fmod(numerator, (double) 6));
  }

  else if(cmax == g_prime){
      h = 60*((b_prime - r_prime)/delta + 2);
  }
  else if(cmax == b_prime){
      h = 60*((r_prime - g_prime)/delta + 4);
  }
  else{
    h = 0;
  }

  if (cmax == 0){
    s = 0;
  }
  else{
    s = delta/cmax;
  }

  v = cmax;
  ////////////////////////////////////////////////////////////////////////////
}
