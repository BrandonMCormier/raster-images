#include "hsv_to_rgb.h"
#include <algorithm>
#include <cmath>

void hsv_to_rgb(
  const double h,
  const double s,
  const double v,
  double & r,
  double & g,
  double & b)
{
  ////////////////////////////////////////////////////////////////////////////
  // Replace with your code here:

  double c = v * s;
  double x = c* (1- std::fabs(std::fmod(h/60,2) - 1));
  double m = v - c;
  double r_prime = 0;
  double g_prime = 0;
  double b_prime = 0;

  //https://www.rapidtables.com/convert/color/hsv-to-rgb.html
  //https://en.wikipedia.org/wiki/HSL_and_HSV

  if(h >= 0 && h < 60){
    r_prime = c;
    g_prime = x;
    b_prime = 0;
  }
  else if(h >= 60 && h < 120){
    r_prime = x;
    g_prime = c;
    b_prime = 0;
  }
  else if(h >= 120 && h < 180){
    r_prime = 0;
    g_prime = c;
    b_prime = x;
  }
  else if(h >= 180 && h < 240){
    r_prime = 0;
    g_prime = x;
    b_prime = c;
  }
  else if(h >= 240 && h < 300){
    r_prime = x;
    g_prime = 0;
    b_prime = c;
  }
  else if(h >= 300 && h < 360){
    r_prime = c;
    g_prime = 0;
    b_prime = x;
  }

  r = (r_prime + m) * 255;
  g = (g_prime + m) * 255;
  b = (b_prime + m) * 255;

  ////////////////////////////////////////////////////////////////////////////
}
