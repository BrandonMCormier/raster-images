#include "rgba_to_rgb.h"

void rgba_to_rgb(
  const std::vector<unsigned char> & rgba,
  const int & width,
  const int & height,
  std::vector<unsigned char> & rgb)
{
  rgb.resize(height*width*3);
  ////////////////////////////////////////////////////////////////////////////
  // Add your code here

  //Just go through each pixel set and omit the 4th value of rgba
  int y = 0;

  for(int i = 0; i < height*width*3; i += 3, y += 4){
    rgb[i] = rgba[y];
    rgb[i+1] = rgba[y+1];
    rgb[i+2] = rgba[y+2];
  }
  ////////////////////////////////////////////////////////////////////////////
}
