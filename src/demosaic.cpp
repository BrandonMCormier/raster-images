#include "demosaic.h"

void demosaic(
  const std::vector<unsigned char> & bayer,
  const int & width,
  const int & height,
  std::vector<unsigned char> & rgb)
{
  rgb.resize(width*height*3);
  ////////////////////////////////////////////////////////////////////////////
  // Add your code here
  	int rgb_pos = 0;
  	for(int i = 0; i < height; i++){
		for(int j = 0; j<width; j++){
			int total_red = 0;
			int count_red = 0;
			int total_blue = 0;
			int count_blue = 0;
			int total_green = 0;
			int count_green = 0;

			//average out the colors

			for(int first = -1; first < 2; first ++){
				for(int second = -1; second <2; second ++){
					int neighbour_x = j + first;
					int neighbour_y = i +second;
					if(neighbour_x >= 0 && neighbour_x < width && neighbour_y >= 0 && neighbour_y < height){
						int bayer_position = neighbour_x%2 + neighbour_y%2;
						switch(bayer_position) {
						case 0:
						    total_blue += bayer[neighbour_x + neighbour_y*width];
						    count_blue ++;
						    break; 

						case 1:
						    total_green += bayer[neighbour_x + neighbour_y*width];
						    count_green ++;
						    break; 

						case 2:
						    total_red += bayer[neighbour_x + neighbour_y*width];
						    count_red ++;
						    break; 
						}
					}
				}
			}


			//here, we use a switch to either use the color available or 
			//use the averages
			
			int bayer_position = i%2 + j%2;
			int position = j+width*i;
			unsigned char red = (unsigned char)((double)total_red/count_red);
			unsigned char green = (unsigned char)((double)total_green/count_green);
			unsigned char blue = (unsigned char)((double)total_blue/count_blue);
			switch(bayer_position) {
				case 0:
				    rgb[rgb_pos ++] = red;
                	rgb[rgb_pos ++] = green;
                	rgb[rgb_pos ++] = bayer[position];
				    break; 

				case 1:
				    rgb[rgb_pos ++] = red;
                	rgb[rgb_pos ++] = bayer[position];
                	rgb[rgb_pos ++] = blue;
				    break; 

				case 2:
				    rgb[rgb_pos ++] = bayer[position];
                	rgb[rgb_pos ++] = green;
                	rgb[rgb_pos ++] = blue;
				    break; 
			}
		}
	}
  ////////////////////////////////////////////////////////////////////////////
}
